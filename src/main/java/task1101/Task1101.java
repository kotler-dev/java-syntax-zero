package task1101;

import static task1101.SolarSystem.*;

public class Task1101 {
    public static void main(String[] args) {
        System.out.println("Человечество живет в Солнечной системе.");
        System.out.printf("Ее возраст около %d лет.\n", age);
        System.out.printf("В Солнечной системе %d известных планет.\n", planetsCount);
        System.out.printf("Как и большинство звездных систем, состоит из %d звезды.\n", starsCount);
        System.out.printf("Звезды по имени %s.\n", starName);
        System.out.printf("Расстояние к центру галактики составляет %d световых лет.\n", galacticCenterDistance);
        System.out.println("Каждый обитатель Солнечной системы должен знать эту информацию!");
    }
}

