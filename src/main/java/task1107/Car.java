package task1107;

public class Car {
    Engine engine = new Engine();

    class Engine {
        private boolean isRunning = false;

        public boolean getRunning() {
            return isRunning;
        }

        public void setRunning(Boolean running) {
            isRunning = running;
        }

        public void start() {
            setRunning(true);
        }

        public void stop() {
            setRunning(false);
        }
    }

//    static Engine engine = new Engine();

    public static void main(String[] args) {
        Car car = new Car();

        System.out.println(car.engine.getRunning());
        car.engine.start();
        System.out.println(car.engine.getRunning());
        car.engine.stop();
        System.out.println(car.engine.getRunning());
    }
}
