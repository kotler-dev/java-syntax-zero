package task1212;

import java.lang.reflect.Array;
import java.util.Arrays;

public class CustomStringArrayList {

    private int size;
    private int capacity;
    private String[] elements;

    public CustomStringArrayList() {
        capacity = 10;
        size = 0;
        elements = new String[capacity];
    }

    public void add(String element) {
        if (size == capacity) {
            grow();
        }
        elements[size] = element;
        size++;
    }

    private void grow() {
        capacity = elements.length + (elements.length / 2);
        elements = Arrays.copyOf(elements, capacity);
    }

    @Override
    public String toString() {
        return "CustomStringArrayList{" +
                "elements=" + Arrays.toString(elements) +
                '}';
    }
}
