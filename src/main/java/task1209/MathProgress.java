package task1209;

import java.util.Arrays;

public class MathProgress {
    public static void main(String[] args) {
        int[] arr1 = new int[]{1, 2, 3, 4, 5, 6, 6, 7, 8, 9, 10};
        int[] arr2 = new int[]{1, 2, 3, 4, 5, 6, 7, 7, 8, 9};

        System.out.println(Arrays.stream(arr1).sum() - Arrays.stream(arr2).sum()); // Solution 1
        System.out.println("arr1 -> " + (Arrays.stream(arr1).sum() - (11 * (11 - 1) / 2))); // Solution 2
        System.out.println("arr2 -> " + (Arrays.stream(arr2).sum() - (10 * (10 - 1) / 2)));
        int[] arr = new int[]{1, 3, 5, 7, 9, 5, 10, 42, 18, 0};
    }
}

