package task1109;

//import task1109.;

//1. создать объект внешнего класса;
//2. Класс родитель.Класс внутренний имя переменной = имя переменной содержащей ссылку на объект
// внешнего класса.новый объект Внутреннего класса (создание объекта внутр. класса)
//3. создание объекта вложенного класса
//Ссылка на полезную статью у Леши внизу.

class Outer {
    public static class Nested {
    }

    class Inner {
    }
}

public class Solution {
    public static void main(String[] args) {
        Outer outer = new Outer();
        Outer.Inner inner = outer.new Inner();
        Outer.Nested nested = new Outer.Nested();
    }
}
