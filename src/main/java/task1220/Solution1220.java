package task1220;

import task1212.CustomStringArrayList;

import java.util.ArrayList;

public class Solution1220 {
    public static void main(String[] args) {
        var elements = new ArrayList<>();
        elements.add("Привет");
        elements.add(10);
        elements.add(10.1);
        elements.add(new Integer[15]);
        elements.add(new LinkageError());

        checkElementsType(elements);
    }

    public static void checkElementsType(ArrayList<Object> elements) {
        boolean flag = false;
        for (Object obj : elements) {
            if (obj instanceof String) {
                printString();
                flag = true;
            }
            if (obj instanceof Integer) {
                printInteger();
                flag = true;
            }
            if (obj instanceof Integer[]) {
                printIntegerArray();
                flag = true;
            }
            if (!flag) {
                printUnknown();
            }
            flag = false;
        }
    }

    public static void printString() {
        System.out.println("Строка");
    }

    public static void printInteger() {
        System.out.println("Целое число");
    }

    public static void printIntegerArray() {
        System.out.println("Массив целых чисел");
    }

    public static void printUnknown() {
        System.out.println("Другой тип данных");
    }
}
