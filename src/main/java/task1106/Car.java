package task1106;

public class Car {
    private String modelName;
    private int speed;

    public String getModelName() {
        return this.modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public static void main(String[] args) {
        Car car = new Car();
        Car car2 = new Car();
        car.setModelName("aa");
        car2.setModelName("bb");
        System.out.println(car.getModelName());
        System.out.println(car2.getModelName());
    }
}

